# frozen_string_literal: true

Gem::Specification.new do |gem|
  gem.add_dependency 'httparty', '~> 0.13'
  gem.add_dependency 'json', '>= 1.8'
  gem.add_dependency 'jwt'

  gem.add_development_dependency 'hint-rubocop_style'
  gem.add_development_dependency 'pry'
  gem.add_development_dependency 'rspec'
  gem.add_development_dependency 'rspec_junit_formatter'
  gem.add_development_dependency 'simplecov'
  gem.add_development_dependency 'webmock'

  gem.authors       = ['Dinesh Gadge']
  gem.email         = ['dinesh@coralmango.com']
  gem.description   = 'A Ruby API wrapper for zoom.us API'
  gem.summary       = 'zoom.us API wrapper'
  gem.homepage      = 'https://gitlab.com/dgit-gems/zoom-ruby-client'
  gem.licenses      = ['MIT']

  gem.files         = `git ls-files`.split($OUTPUT_RECORD_SEPARATOR)
  gem.executables   = gem.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = 'zoom-ruby-client'
  gem.require_paths = ['lib']
  gem.version       = '0.8.2'
end
